const mocha = require('mocha');
const assert = require('chai').assert;

const binarySearch = require('./index');

describe('Check algorithm of binary search', () => {
   it('should correct work with valid input', () => {
       //общая проверка
       assert.equal(binarySearch([1,2,3],3),2);
       //общая с большим числом данных на каждый элемент
       assert.equal(binarySearch([1,2,3,5,6,7,8],7),5);
       assert.equal(binarySearch([1,2,3,5,6,7,8],1),0);
       assert.equal(binarySearch([1,2,3,5,6,7,8],2),1);
       assert.equal(binarySearch([1,2,3,5,6,7,8],3),2);
       assert.equal(binarySearch([1,2,3,5,6,7,8],5),3);
       assert.equal(binarySearch([1,2,3,5,6,7,8],6),4);
       assert.equal(binarySearch([1,2,3,5,6,7,8],8),6);
       //проверка на нуль
       assert.equal(binarySearch([0,0,0],0),1);
       //проверка при пол, отриц и нуль
       assert.equal(binarySearch([-1,0,1],-1),0);
       //проверка на отрицательные
       assert.equal(binarySearch([-9,-7,-6],-6),2);
       //проверка на среднее значение
       assert.equal(binarySearch([-12,-10,-9,-8,-7,-6,-5,-4,0],-7),4);
   });
   it('should work correctly with corner cases', function () {
       //проверка на максимально возможное числовое значение
       assert.equal(binarySearch([1,2,3, 9007199254740990, 9007199254740991,9007199254740992],9007199254740991),4);
       // на минимальное и максимальное
       assert.equal(binarySearch([-9007199254740992,-9007199254740,-900719925,1,2,3, 9007199254740990, 9007199254740991,9007199254740992],-9007199254740992),0);
       assert.equal(binarySearch([0.9007199254740000, 0.9007199254740991000, 0.9007199254740992000,0.9007199254740992000],0.9007199254740992000),2);
       assert.equal(binarySearch([-0.987654321987621,-0.98765432198732,-0.98765432198731,-0.9876543219843],-0.9876543219843),3);
   });
    it('should return -1 with incorrect values', function () {
        assert.equal(binarySearch(['a','b','c'],9007199254740991),-1);
        assert.equal(binarySearch([],9007199254740991),-1);
        assert.equal(binarySearch([{},{}],9007199254740991),-1);
        assert.equal(binarySearch({},9007199254740991),-1);
        assert.equal(binarySearch([1,2,3],'a'),-1);
        assert.equal(binarySearch([1,2,3],[]),-1);
        assert.equal(binarySearch([1,2,3],{}),-1);
        assert.equal(binarySearch([1,2,3],[1,2,3]),-1);
        assert.equal(binarySearch([1,2,3], new Date('December 17, 1995 03:24:00')),-1);
    });
    it('should work correctly with unsorted array', function () {
        assert.equal(binarySearch([3,2,1],3), 2);
        assert.equal(binarySearch([343212,-234310,-2343249,238,234327,36,-2343245,-2344,0],-2343249), 0);
        assert.equal(binarySearch([4,3,-10,-5],-5), 1);
    });
});