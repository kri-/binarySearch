const checkData = (array, value) => {
    const regExp = /\.*\d|-d*/g;
    if (!array || array.length === 0 || !Array.isArray(array)) {
        return -1;
    }
    if (array.join('').replace(regExp,'') !== "") {
        return -1;
    }
    if (typeof value != "number") {
        return -1;
    }
    return 0;
};

const binarySearch = (array, value) => {
    if (checkData(array,value) === -1) {
        return -1;
    }
    array.sort((left,right) => {
       return left - right;
    });
    const find = (leftValue, rightValue) => {
        const middleKey = Math.floor((leftValue + rightValue)/2);
        const middleValue = array[middleKey];
        if (middleValue === value) {
            return middleKey;
        }
        if (middleValue > value) {
            return find(leftValue, middleKey);
        }
        if (middleValue < value) {
            return find(middleKey+1, rightValue);
        }
        // return (middleValue === value ? middleKey :
        // (middleValue > value ? find(leftValue, middleKey) :
        // find(middleKey+1, rightValue)));
    };
    return find(0, array.length);
};

module.exports = binarySearch;